import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

// import Modals from './Modals/Reducer';
import bounds from './reducers/bounds';
import pathHistory from './reducers/pathHistory';

import home from '../components/PersonalHome/Reducer';
import quotes from '../components/QuoteViewer/Reducer';
import blog from '../components/Blog/reducer';

// resizeListener reducer

const reducer = combineReducers({
  pathHistory,
  bounds,
  home,
  router: routerReducer,
  quotes,
  blog
});

export default reducer;
