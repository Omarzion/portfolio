export const container: string;
export const spacer: string;
export const innerContainer: string;
export const logo: string;
export const tray: string;
export const left: string;
export const link: string;
export const right: string;
export const active: string;
export const hamburger: string;
export const bar: string;
export const trayOpen: string;
