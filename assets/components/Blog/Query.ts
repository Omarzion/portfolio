export interface Query {
  limit?: number,
  start?: number;
  filters?: {
    [x:string]: string;
  }
  sort?: {
    by: string,
    dir: 'asc' | 'desc';
  }
  model: string;
}

const filterMap = [
  // Equals
  ['==', ''],
  ['===',''],
  ['=', ''],

  // Not Equals
  ['!==', '_ne'],
  ['!=', '_ne'],

  ['<', '_lt'],
  ['>', '_gt'],
  ['<=', '_lte'],
  ['>=', '_gte'],
  ['[]', '_contains'],
  ['{}, _containss']

]

const translateFilter = query =>
  filterMap.reduce((result, filter) =>
    result.indexOf(filter[0]) === 0
      ? `${filter[1]}=${result.slice(filter[0].length).trim()}`
      : result
    ,query);

export default function strapiQuery({filters = {}, model, ...rest}: Query): string {
  let query = `${ENV.API_ROOT}/${model}?`;
  let params = [
    ...Object.keys(filters).map(filter => `${filter}${translateFilter(filters[filter])}`),
    rest.limit ? `_limit=${rest.limit}` : null,
    rest.start ? `_start=${rest.start}` : null,
    rest.sort ? `_sort=${rest.sort!.by}:${rest.sort!.dir}` : null
  ]
  query += encodeURI(params.filter(p => p).join('&'));
  return query;
}
