import { combineReducers } from 'redux';
import * as Actions from './actions';
import initialState from './viewModel';



function isFetching(state = false, action) {
  switch(action.type) {
    case Actions.FETCH.RECENT.SUCCESS:
    case Actions.FETCH.POST.SUCCESS:
    case Actions.FETCH.TAG.SUCCESS:
      return false;
    case Actions.FETCH.RECENT.PENDING:
    case Actions.FETCH.TAG.PENDING:
    case Actions.FETCH.POST.PENDING:
    case Actions.FETCH.RECENT.ERROR:
    case Actions.FETCH.POST.ERROR:
    case Actions.FETCH.TAG.ERROR:
      return true;
    default:
      return state;
  }
}


function posts(state = initialState.posts, action) {
  switch(action.type) {
    case Actions.FETCH.RECENT.SUCCESS:
    case Actions.FETCH.TAG.SUCCESS:
      return action.data;
      // return action.data.reduce((posts, tag) => [...posts, ...tag.posts], []);
    default:
      return state;
  }
}

function post(state = initialState.post, action) {
  switch(action.type) {
    case Actions.FETCH.POST.SUCCESS:
      return action.data[0];
    default:
      return state;
  }
}

function query(state = initialState.query, action) {
  switch(action.type) {
    case Actions.FETCH.TAG.SUCCESS:
      return action.meta;
    default:
      return state;
  }
}


export default combineReducers({
  posts,
  post,
  query
})
