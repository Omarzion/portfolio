
export interface IBlogProps extends IConntected {
  posts: IPost[];
}

export interface IBlogPostProps extends IConntected {
  post: IPost;
}

export interface IDispatch {
  dispatch: Function;
}

export interface IRecord {
  id: string;
  _id: string;
  updatedAt: Date;
  createdAt: Date;
}


export interface IPost extends IRecord {
  images: string[];
  title: string;
  slug: string;
  stub: string;
  content: string;
  published: boolean;
  publishedOn: Date;
  categories: ICategory[];
  author: IAuthor;
  estimatedReadingTime: string;
}

export interface ICategory extends IRecord {
  name: string;
  key: string;
}

export interface IAuthor extends IRecord {
  username: string;
  email: string;
  provider: string;
}

export default {
  posts: [],
  post: null,
  query: { model: 'blogpost' }
};



// Generic default interfaces

export interface IConntected {
  location: ILocation;
  match: IReactRouterMatch;
  dispatch: Function;
  history: IHistory;
}

export interface IHistory {
  action: string;
  block: Function;
  createHref: Function;
  go: Function;
  goBack: Function;
  goForward: Function;
  listen: Function;
  replace: Function;
  push: Function;
  location: ILocation;
  length: number;
}

export interface IReactRouterMatch {
  isExact: boolean;
  params: {
    [key: string]: any;
  },
  path: string;
  url: string;
}

export interface ILocation {
  hash: string;
  pathname: string;
  search: string;
}