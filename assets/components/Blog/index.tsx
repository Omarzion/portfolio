import * as React from 'react';
import { connect } from 'react-redux';

import DynamicImage from '../DynamicImage/DynamicImage';

import Container from '../Container/Container';

import * as style from './blog.scss';
import * as Actions from './actions';
import { IBlogProps } from './viewModel';

import { Stub, PostContainer } from './Post/pieces';
import { Url } from './utils';



class Blog extends React.Component<IBlogProps> {

  componentDidMount() {
    const { dispatch, location: { search }} = this.props;
    if (search) {
      dispatch(Actions.fetchTag(Url.parseSearch(search)))
    } else {
      dispatch(Actions.fetchRecent());
    }
  }

  render() {
    const { posts, dispatch } = this.props;
    return (
      <PostContainer>
      <div className={style.spacer} />
        {posts.map(post => <Stub key={post._id} dispatch={dispatch} {...post} />)}
      </PostContainer>
    )
  }
}




export default connect(state => ({...state.blog}))(Blog);
