import query, { Query } from './Query';

export const FETCH = {
  RECENT: {
    PENDING: 'BLOG_FETCH_RECENT_PENDING',
    SUCCESS: 'BLOG_FETCH_RECENT_SUCCESS',
    ERROR: 'BLOG_FETCH_RECENT_ERROR',
  },
  TAG: {
    PENDING: 'BLOG_FETCH_TAG_PENDING',
    SUCCESS: 'BLOG_FETCH_TAG_SUCCESS',
    ERROR: 'BLOG_FETCH_TAG_ERROR',
  },
  POST: {
    PENDING: 'BLOG_FETCH_POST_PENDING',
    SUCCESS: 'BLOG_FETCH_POST_SUCCESS',
    ERROR: 'BLOG_FETCH_POST_ERROR',
  }
}

export function fetchRecent(page = 0) {
  let search: Query = {
    sort: { by: 'createdAt', dir: 'desc' },
    limit: 10,
    start: page,
    model: 'blogpost'
  }
  const t = FETCH.RECENT;
  return {
    type: [ t.PENDING, t.SUCCESS, t.ERROR],
    url: query(search)
  }
}

export function fetchPost(slug) {

  let search:Query = {
    filters: { slug: `=${slug}` },
    model: 'blogpost'
  }

  const t = FETCH.POST;
  return {
    type: [t.PENDING, t.SUCCESS, t.ERROR],
    url: query(search)
  }
}

export function fetchTag(tag) {
  const t = FETCH.TAG;
  let search: Query = {
    model: 'blogpost/tags',
    filters: { 'categories._id': '='+tag }
  }
  return {
    type: [t.PENDING, t.SUCCESS, t.ERROR],
    url: query(search),
    meta: search
  }
}