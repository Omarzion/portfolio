export class Url {
  public static parseSearch(search: string) {
    if (!search.length || search[0] !== '?') return {};
    return search
      .slice(1)
      .split('&')
      .reduce((o: any, next: string) => {
        let pair: any[] = next.split('=');
        pair[1] = pair[1] || true;

        return {...o, [pair[0]]: pair[1]};
      }, {});
  }
}