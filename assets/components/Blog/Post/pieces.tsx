import * as React from 'react';
import * as style from './pieces.scss';
import { DateTime } from 'luxon';

import { Link } from 'react-router-dom';

// import * as style from './Stub.scss';

import { IPost, ICategory, IDispatch } from '../viewModel';
import Markdown from './Markdown';
import * as Actions from '../actions';

export function Category(props: ICategory & IDispatch) {
  return (
    <span onClick={() => props.dispatch(Actions.fetchTag(props._id))} className={style.category} key={props._id}>#{props.name}</span>    
  )
}

export function Stub({ dispatch, _id, title, slug, author, categories, images, stub, estimatedReadingTime }: IPost & IDispatch) {
  return (
    <div className={style.stubContainer}>
      <Link className={style.link} to={`/blog/post/${slug}`}><h2 className={style.title}>{title}</h2></Link>
      <div>
        <p className={style.author}>by {author.username} &#x2015; {estimatedReadingTime}</p>
      </div>
      <Markdown source={stub} />
      <div className={style.flexSplit}>
        <Link style={{textDecoration: 'none'}} to={`/blog/post/${slug}`}>Continue Reading</Link>
        <div>
          {categories.map(category => <Category {...category} dispatch={dispatch} />)}
        </div>
      </div>
    </div>
  )
}

export function PostDetail({post}) {
  if (!post) {
    return <div>Loading</div>
  }
  return (
    <div className={style.stubContainer}>
      <InlinePostMeta {...post} />
      <h2 className={style.title}>{post.title}</h2>
      <p className={style.author}>by {post.author.username}</p>
      {post.categories.map(Category)}
      <Markdown source={post.content}/>
      
    </div>
  )
}

export function PostContainer(props) {
  return (
    <div className={style.postContainer}>
      <div {...props} />
    </div>
  )
}

export function PostMeta(props: IPost) {
  let editsSincePublish = Math.floor(DateTime.fromISO(props.updatedAt).diff(DateTime.fromISO(props.publishedOn), 'days')) > 0;
  return (
    <div className={style.postMetaContainer}>
      <table>
        <tbody style={{float: 'left'}}>
          {Row('icon-git-merge', 'Published', formatDate(props.publishedOn))}
          {editsSincePublish ? Row('icon-git-pull-request', 'Updated', formatDate(props.updatedAt)) : null}
          {Row('icon-user', 'Written by', props.author.username)}
          {Row('icon-watch', 'Estimated', props.estimatedReadingTime)}
        </tbody>
      </table>
    </div>
  )
}

export function InlinePostMeta(props: IPost) {
  let editsSincePublish = Math.floor(DateTime.fromISO(props.updatedAt).diff(DateTime.fromISO(props.publishedOn), 'days')) > 0;
  return (
    <div className={style.inlinePostMetaContainer} style={{display: 'flex'}}>
      {/* <table> */}
        {/* <tbody style={{float: 'left'}}> */}
          {Row('icon-git-merge', 'Published', formatDate(props.publishedOn))}
          {editsSincePublish ? Row('icon-git-pull-request', 'Updated', formatDate(props.updatedAt)) : null}
          {/* {Row('icon-user', 'Written by', props.author.username)} */}
          {Row('icon-watch', 'Estimated', props.estimatedReadingTime)}
        {/* </tbody> */}
      {/* </table> */}
    </div>
  )
}

export function Row(icon, key, value) {
  return (
    <div title={`${key} ${value}`}>
      <span><span className={icon} />{/*<span>{key}</span>*/}</span>
      <span>{value}</span>
    </div>
  )
}

function formatDate(date) {
  return DateTime.fromISO(date).toFormat('MM/dd/yyyy');
}