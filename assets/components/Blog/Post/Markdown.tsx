import * as React from 'react';
import * as Markdown from 'react-markdown';
import * as style from './Markdown.scss';
import cx from 'classnames';
import * as Prism from 'prismjs';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-nginx';
import 'prismjs/plugins/autolinker/prism-autolinker';
import 'prismjs/plugins/command-line/prism-command-line';
import 'prismjs/plugins/line-numbers/prism-line-numbers';

import '../../../scss/prism-onedark.css';

// console.log(blah);
// console.log(Prism);
// blah.languages_path = 'https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/components/';
// Prism.plugins.autoloader.languages_path = 'https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/components/';

export default props => (
  <Markdown
    className={style.container}
    renderers={{
      code: ({value, language}) => <pre className={language}><PrismBlock children={value} language={language}/></pre>,
      inlineCode: ({value, language}) => <PrismBlock children={value} language={language}/>,
      heading: Heading,
      link: Link
    }}
    {...props}
  />
)

function cleanSelfReference(s) {
  return s.toString().toLowerCase().replace(/[^a-z0-9\-]/g, '-').replace(/^[0-9\s\uFEFF\xA0\-]+|[\s\uFEFF\xA0\-]+$/g, '');
}

function Link(props) {
  let href = props.href;
  if (href.indexOf('#') === 0) {
    href = '#' + cleanSelfReference(href.slice(1));
  }
  return <a href={href} children={props.children} />
}

function Heading(props) {
  let C = `h${props.level}`;
  return <C id={cleanSelfReference(props.children)}>{props.children}</C>
}


class PrismBlock extends React.PureComponent<{language?: string}> {
  el;

  getLanguageCode(str) {
    let match = /.+?\s/.exec(str);
    return match && match[0].trim();
  }

  render () {
    const className = cx({
      'react-prism': true,
      [`language-${this.props.language}`]: !!this.props.language,
      [`${this.props.language}`]: !!this.props.language,
      'line-numbers': true
    })

    return (
        <code ref={ref => (this.el = ref)} className={className}>
          {this.props.children}
        </code>
    )
  }

  componentDidMount () {
    this.highlightCode()
  }

  componentDidUpdate () {
    this.highlightCode()
  }

  highlightCode () {
    Prism.highlightElement(this.el);
  }
}

// rgb(0, 0, 0) 0px 0px 8px