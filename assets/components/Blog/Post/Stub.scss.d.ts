export const container: string;
export const title: string;
export const author: string;
export const link: string;
