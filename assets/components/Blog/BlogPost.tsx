import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { IBlogPostProps } from './viewModel';
import { PostDetail, PostContainer } from './Post/pieces';
import * as Actions from './actions';
import * as style from './blog.scss';
import { goBack } from 'react-router-redux';


class BlogPost extends React.Component<IBlogPostProps> {
  componentDidMount() {
    const { match: { params }} = this.props;
    console.log(this.props);
    this.props.dispatch(Actions.fetchPost(params.slug));
  }

  render() {
    const { post, dispatch } = this.props;
    return (
      <PostContainer>
        <div className={style.spacer} />
        <div className={style.link} onClick={() => dispatch(goBack())}><span className="icon-chevron-left" /> Back</div>
        <PostDetail post={post} />
      </PostContainer>
    )
  }
}

export default connect(state => state.blog)(BlogPost);