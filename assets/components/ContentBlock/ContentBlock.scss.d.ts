export const container: string;
export const imageContent: string;
export const heading: string;
export const subheading: string;
