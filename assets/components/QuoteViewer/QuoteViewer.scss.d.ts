export const background: string;
export const container: string;
export const quoteContainer: string;
export const bottomContainer: string;
export const nextQuote: string;
export const icon: string;
