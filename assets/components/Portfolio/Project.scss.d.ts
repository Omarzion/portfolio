export const container: string;
export const innerContainer: string;
export const name: string;
export const contentContainer: string;
export const description: string;
export const selected: string;
export const details: string;
